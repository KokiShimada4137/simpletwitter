package chapter6.controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import chapter6.service.MessageService;

@WebServlet(urlPatterns = { "/deleteMessage" })
public class DeleteMessageServlet extends HttpServlet {

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws IOException, ServletException {

    	//削除ボタンが押されたメッセージのidをmessageIdに格納
        int messageId = Integer.valueOf(request.getParameter("messageId"));

        //MessageServiceを呼び出す
        new MessageService().delete(messageId);

        //処理が完了したらtop画面にリダイレクトする
        response.sendRedirect("./");
    }
}