package chapter6.controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import chapter6.beans.User;
import chapter6.beans.UserComment;
import chapter6.beans.UserMessage;
import chapter6.service.CommentService;
import chapter6.service.MessageService;

@WebServlet(urlPatterns = { "/index.jsp" })
public class TopServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
        throws IOException, ServletException {

    	//sessionからログインユーザを取得し、userに格納
        boolean isShowMessageForm = false;
        User user = (User) request.getSession().getAttribute("loginUser");

        //userが空でなければisShowMessageFormをtrueにする
        if (user != null) {
            isShowMessageForm = true;
        }

        //絞り込み機能の開始日、終了日を取得
        String start = request.getParameter("startDate");
        String end = request.getParameter("endDate");

        // 実践問題② user_idを読み込み変数userIdに格納
        String userId = request.getParameter("user_id");

        // 実践問題② userIdを引数に追加
        List<UserMessage> messages = new MessageService().select(userId, start, end);

        List<UserComment> comments = new CommentService().select();

        //値を格納
        request.setAttribute("messages", messages);
        request.setAttribute("comments", comments);
        request.setAttribute("isShowMessageForm", isShowMessageForm);
        request.setAttribute("startDate", start);
        request.setAttribute("endDate", end);
        request.getRequestDispatcher("/top.jsp").forward(request, response);

    }
}