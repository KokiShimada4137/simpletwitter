<%@page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<% pageContext.setAttribute("newLineChar", "\n"); %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<title>簡易Twitter</title>
		 <link href="./css/style.css" rel="stylesheet" type="text/css">
	</head>

	<body>
		<div class="main-contents">

			<%-- ヘッダをここに表示 --%>
			<div class="header">

				<%-- loginUserが空であれば表示 --%>
				<c:if test="${ empty loginUser }">
					<a href="login">ログイン</a>
					<a href="signup">登録する</a>
				</c:if>

				<%-- loginUserが空でなければ表示 --%>
				<c:if test="${ not empty loginUser }">
					<a href="./">ホーム</a>
					<a href="setting">設定</a>
					<a href="logout">ログアウト</a>
				</c:if>

			</div>

			<%-- loginUserが空でなければloginUserの情報を表示 --%>
			<c:if test="${ not empty loginUser }">
				<div class="profile">
					<div class="name"><h2><c:out value="${loginUser.name}" /></h2></div>
					<div class="account">@<c:out value="${loginUser.account}" /></div>
					<div class="description"><c:out value="${loginUser.description}" /></div>
				</div>
			</c:if>

			<%-- errorMessagesが空でなければエラーメッセージを表示 --%>
			<c:if test="${ not empty errorMessages }">
				<div class="errorMessages">
        			<ul>
            			<c:forEach items="${errorMessages}" var="errorMessage">
                		<li><c:out value="${errorMessage}" />
            			</c:forEach>
        			</ul>
    			</div>

    			<%-- sessionからerrorMessagesを削除 --%>
				<c:remove var="errorMessages" scope="session" />
			</c:if>

			<%-- 日付の絞り込み機能 --%>
			<form action="index.jsp" method="GET">
				日付：<input type="date" name="startDate" value="${startDate}"> ～ <input type="date" name="endDate" value="${endDate}">
				<input type="submit" value="絞り込み">
			</form>

			<%-- loginUserが空でなければつぶやきフォームを表示 --%>
			<c:if test="${ isShowMessageForm }">

			<div class="form-area">
        			<form action="message" method="post">
            			いま、どうしてる？<br />
            			<textarea name="text" cols="100" rows="5" class="tweet-box"></textarea>
            			<br />
            			<input type="submit" value="つぶやく">（140文字まで）
        			</form>
			</div>

			</c:if>

			<%-- メッセージフォームの表示 --%>
			<div class="messages">

				<%-- 取得したmessagesを繰り返し表示 --%>
   				 <c:forEach items="${messages}" var="message">
        			<div class="message">
            		<div class="account-name">
                		<span class="account"><c:out value="${message.account}" /></span>
                		<span class="account">
							<a href="./?user_id=<c:out value="${message.userId}"/>&startDate=<c:out value="${startDate}"/>&endDate=<c:out value="${endDate}"/> ">
        						<c:out value="${message.account}" />
    						</a>
						</span>
                		<span class="name"><c:out value="${message.name}" /></span>
            		</div>

            		<div class="text">
            			<c:forEach var="s" items="${fn:split(message.text, newLineChar)}">
    						<div>${s}</div>
						</c:forEach>
            		</div>

            		<div class="date"><fmt:formatDate value="${message.createdDate}" pattern="yyyy/MM/dd HH:mm:ss" /></div>

					<%-- messageのuserIdとログインユーザのIdが同じであれば削除、編集ボタンを表示 --%>
					<c:if test = "${ message.userId == loginUser.id }">
            			<form action="deleteMessage" method="post">
            				<input type="hidden" name ="messageId" value="${message.id}">
            				<input type="submit" value="削除">
        				</form>

        				<form action="edit" method="get">
            				<input type="hidden" name ="messageId" value="${message.id}">
            				<input type="submit" value="編集">
        				</form>

        			</c:if>

        			<%-- 返信コメントをここに表示 --%>
        			<c:forEach items="${comments}" var="comment">
        				<c:if test = "${ comment.messageId == message.id }">
        			    <div class="account-name">
                			<span class="account"><c:out value="${comment.account}" /></span>
                			<span class="account">
								<a href="./?user_id=<c:out value="${message.userId}"/>&startDate=<c:out value="${startDate}"/>&endDate=<c:out value="${endDate}"/> ">
        							<c:out value="${comment.account}" />
    							</a>
							</span>
                			<span class="name"><c:out value="${comment.name}" /></span>
            			</div>

            			<div class="text">
            			<c:forEach var="s" items="${fn:split(comment.text,newLineChar)}">
    						<div>${s}</div>
						</c:forEach>
            			</div>

            			<div class="date"><fmt:formatDate value="${comment.createdDate}" pattern="yyyy/MM/dd HH:mm:ss" /></div>
            			</c:if>
        			</c:forEach>

					<%-- loginUserが空でなければ返信フォームを表示 --%>
					<c:if test="${ isShowMessageForm }">
	        			<form action="comment" method="post">
    	        			<textarea name="text" cols="100" rows="5" class="tweet-box"></textarea>
        	    			<br />
        	    			<input type="hidden" name ="messageId" value="${message.id}">
        	    			<input type="hidden" name ="userId" value="${loginUser.id}">
        	    			<input type="submit" value="返信">（140文字まで）
        				</form>
        			</c:if>

        			</div>
    			</c:forEach>

			</div>


			<div class="copyright"> Copyright(c)YourName</div>
		</div>
	</body>

</html>