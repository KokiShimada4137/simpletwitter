package chapter6.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import chapter6.beans.Message;
import chapter6.service.MessageService;

@WebServlet(urlPatterns = { "/edit" })
public class EditServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws IOException, ServletException {

        HttpSession session = request.getSession();
        request.getSession().getAttribute("loginUser");


    	//編集ボタンが押されたメッセージのidをmessageIdに格納
        String messageId = request.getParameter("messageId");
        Message message = null;

        //メッセージIDが空もしくは数字であるかの確認
        if(!(StringUtils.isBlank(messageId)) && messageId.matches("^[0-9]+$")) {

        	//MessageServiceを呼び出す
        	//messageIdのメッセージがなかった場合、messageはnullとなる
        	message = new MessageService().select(Integer.valueOf(messageId));

        }

        //messageがnullの場合、errorMessagesに「不正なパラメータが入力されました」を追加
        if(message == null){
        	List<String> errorMessages = new ArrayList<String>();
        	errorMessages.add("不正なパラメータが入力されました");
        	session.setAttribute("errorMessages", errorMessages);
        	response.sendRedirect("./");
        	return;
        }

        //処理が完了したらEdit画面を表示する
        session.setAttribute("message", message);
        request.getRequestDispatcher("/edit.jsp").forward(request,response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws IOException, ServletException {

    	HttpSession session = request.getSession();

    	Message message = getMessage(request);
        List<String> errorMessages = new ArrayList<String>();

        //入力したメッセージのテキストを取得し、バリデーションをかける
        String text = message.getText();
        if (!isValid(text, errorMessages)) {


            session.setAttribute("errorMessages", errorMessages);
            session.setAttribute("text", text);
            request.getRequestDispatcher("/edit.jsp").forward(request,response);

            return;
        }

    	new MessageService().update(message);
    	response.sendRedirect("./");
    }


    //リクエストからmessageを作成
    private Message getMessage(HttpServletRequest request) throws IOException, ServletException {

    	Message message = new Message();
    	message.setId(Integer.parseInt(request.getParameter("messageId")));
    	message.setUserId(Integer.parseInt(request.getParameter("userId")));
    	message.setText(request.getParameter("text"));

        return message;
    }

    private boolean isValid(String text, List<String> errorMessages) {

        if (StringUtils.isBlank(text)) {
            errorMessages.add("メッセージを入力してください");
        } else if (140 < text.length()) {
            errorMessages.add("140文字以下で入力してください");
        }

        if (errorMessages.size() != 0) {
            return false;
        }
        return true;
    }

}
