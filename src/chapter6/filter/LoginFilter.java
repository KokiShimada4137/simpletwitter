package chapter6.filter;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import chapter6.beans.User;


@WebFilter(urlPatterns={"/setting", "/edit"})
public class LoginFilter implements Filter {

	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {

		//もしつぶやき編集画面とユーザ設定画面に遷移するときに、セッションを持っていなければ、ログイン画面に戻す
		User user = (User) ((HttpServletRequest)request).getSession().getAttribute("loginUser");

        if(user != null) {
        	chain.doFilter(request, response);
        }else {
        	HttpSession session = ((HttpServletRequest) request).getSession();
        	List<String> errorMessages = new ArrayList<String>();

        	errorMessages.add("ログインしてください");
            session.setAttribute("errorMessages", errorMessages);
            request.getRequestDispatcher("/top.jsp").forward(request, response);
        }

	}

	@Override
	public void init(FilterConfig fConfig) throws ServletException {
	}

	@Override
	public void destroy() {
	}

}